import { Component } from '@angular/core';
import { DataService } from '../services/data.service';
import { Observable, from, forkJoin, of } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

	constructor(public dataService: DataService) {

	}

	loadTheThingsSlow(){
	
		console.time("requests");

		this.dataService.getThingOne().then((res) => {

			let thingOne = res;
			console.log("thing one loaded");
	
			this.dataService.getThingTwo().then((res) => {

				let thingTwo = res;
				console.log("thing two loaded");

				this.dataService.getThingThree().then((res) => {

					let thingThree = res;
					console.log("thing three loaded");
					console.timeEnd("requests");

					console.log("All done!");
					console.log(thingOne);
					console.log(thingTwo);
					console.log(thingThree);

				});

			});

		});

	}

	loadTheThingsQuick(){

		console.time("requests");

		let promiseOne = this.dataService.getThingOne();
		let promiseTwo = this.dataService.getThingTwo();
		let promiseThree = this.dataService.getThingThree();

		Promise.all([promiseOne, promiseTwo, promiseThree]).then((values) => {
		
			console.timeEnd("requests");
			console.log("thingOne: ", values[0]);
			console.log("thingTwo: ", values[1]);
			console.log("thingThree: ", values[2]);

		});

	}

	loadObservables(){

		let observableOne = from(this.dataService.getThingOne());
		let observableTwo = from(this.dataService.getThingTwo());
		let observableThree = from(this.dataService.getThingThree());

		forkJoin([observableOne, observableTwo, observableThree]).subscribe(res => {
			console.log(res);
		});

	}

}
