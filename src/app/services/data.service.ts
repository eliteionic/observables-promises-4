import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(){
    
  }

	getThingOne(){
		
		return new Promise((resolve, reject) => {

			setTimeout(() => {
				resolve("thing one");
			}, 2000);

		});

	}

	getThingTwo(){

		return new Promise((resolve, reject) => {

			setTimeout(() => {
				resolve("thing two");
			}, 2000);

		});

	}

	getThingThree(){

		return new Promise((resolve, reject) => {

			setTimeout(() => {
				resolve("thing three");
			}, 2000);

		});

  }
  
}
